# Disabled because pylint does not like upper case do_GET
# but we have to do so, otherwise it's not mapped to HTTP
# verb GET etc.
#
# pylint: disable=invalid-name
from typing import Union
from typing import List
from typing import Dict

import json as json_module

from http.server import BaseHTTPRequestHandler

DATA_STORE: Dict = {"received_messages_amount": 0, "received_messages": []}

class MockHandler(BaseHTTPRequestHandler):


    def do_POST(self):
        """Handle POST requests
        """

        DATA_STORE['received_messages_amount'] += 1

        body_size = int(self.headers["Content-Length"])
        body_string = self.rfile.read(body_size)
        sent_body = json_module.loads(body_string.decode('utf-8'))

        message = sent_body["blocks"][0]["text"]["text"]
        DATA_STORE["received_messages"].append(message)

        self._send_json_response(
            {},
            status_code=200,
        )
        return

    def do_MOCK_FLUSH(self):
        """ Handle non-standard HTTP verb 'MOCK_FLUSH', to empty internal cache

        The reason of using a non-standard verb is that we're sure
        not to overlap with existing API mapping

        the URL given with this method is currently not used
        """

        DATA_STORE.clear()
        DATA_STORE['received_messages_amount'] = 0
        DATA_STORE['received_messages'] = []

        self.send_response(204)
        self.end_headers()

    def do_MOCK_DEBUG(self):
        """ Handle non-standard HTTP verb 'MOCK_DEBUG', to output DATA_STORE

        the URL given with this method is currently not used
        """

        self.send_response(200)
        self.send_header("Content-Type", "application/json")
        self.end_headers()

        self.wfile.write(json_module.dumps(DATA_STORE).encode("utf-8"))

    def _send_json_response(self, json: Union[Dict, List], status_code=200, headers={}):
        self._send_raw_json_response(
            json_body=json_module.dumps(json).encode("utf-8"),
            status_code=status_code,
            headers=headers
        )

    def _send_raw_json_response(self, json_body: bytes, status_code=200, headers={}):
        self.send_response(status_code)
        self.send_header("Content-Type", "application/json")
        for name, value in headers.items():
            self.send_header(name, value)
        self.end_headers()
        self.wfile.write(json_body)
