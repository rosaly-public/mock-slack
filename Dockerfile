FROM python:3.11-alpine
COPY mock mock/
ENV  MOCK_SLACK_PORT 8093
EXPOSE 8093
# -u is for unbuffered output
# otherwise you will not directly see the stacktrace in the logs when an error occur
# having it unbuffered ease debugging
CMD ["python", "-u", "-m" , "mock"]
